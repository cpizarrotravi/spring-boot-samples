/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.gitlab.cpizarrotravi.controller;

import io.gitlab.cpizarrotravi.model.User;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author cpizarrotravi
 */
@RestController
@RequestMapping("/user")
public class UserRestController {

  private final AtomicLong index = new AtomicLong(1);
  private final List<User> users = new ArrayList<>();

  @GetMapping
  public Flux<User> listUser() {
    return Flux.fromIterable(users);
  }

  @GetMapping("/{id}")
  public Mono<User> getUser(@PathVariable String id) {
    return Mono.just(id).map(key -> {
      return users.stream()
              .filter(u -> Long.parseLong(id) == u.getId())
              .findFirst().orElse(null);
    });
  }

  @PostMapping
  public Mono<User> newUser(@RequestBody User newUser) {
    return Mono.just(newUser).map(user -> {
      newUser.setId(index.getAndIncrement());
      users.add(newUser);
      return user;
    });
  }

  @PutMapping
  public Mono<User> updUser(@RequestBody User updUser) {
    return Mono.just(updUser).map(user -> {
      int position;
      for (position = 0; position < users.size(); position++) {
        if (user.getId().longValue() == users.get(position).getId()) {
          users.set(position, user);
        }
      }
      return user;
    });
  }

  @DeleteMapping("/{id}")
  public Mono<Void> delUser(@PathVariable String id) {
    return Mono.just(id).flatMap(key -> {
      users.removeIf(u -> Long.parseLong(key) == u.getId());
      return Mono.empty();
    });
  }

}
