package io.gitlab.cpizarrotravi;

import io.gitlab.cpizarrotravi.controller.HelloWorldController;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author cpizarrotravi
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class HelloWorldApplicationTests {

  @Autowired
  private HelloWorldController controller;

  @Test
  public void contextLoads() {
    assertThat(controller).isNotNull();
  }

}
