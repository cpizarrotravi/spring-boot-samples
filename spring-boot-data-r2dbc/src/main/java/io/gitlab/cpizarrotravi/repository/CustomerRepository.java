/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.gitlab.cpizarrotravi.repository;

import io.gitlab.cpizarrotravi.entity.Customer;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author cpizarrotravi
 */
@Repository
public interface CustomerRepository extends ReactiveCrudRepository<Customer, Long> {

}
