/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.gitlab.cpizarrotravi.controller;

import io.gitlab.cpizarrotravi.entity.Customer;
import io.gitlab.cpizarrotravi.service.CustomerService;
import java.time.Duration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

/**
 *
 * @author cpizarrotravi
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {

  @Autowired
  private CustomerService service;

  @GetMapping
  public Flux<Customer> listCustomer() {
    return service.findAllCustomer().delayElements(Duration.ofSeconds(1)).subscribeOn(Schedulers.elastic());
  }

  @GetMapping("/{id}")
  public Mono<Customer> getCustomer(@PathVariable String id) {
    return service.findCustomer(Long.parseLong(id)).subscribeOn(Schedulers.elastic());
  }

  @PostMapping
  public Mono<Customer> newCustomer(@RequestBody Customer customer) {
    return service.insertCustomer(customer).subscribeOn(Schedulers.elastic());
  }

  @PutMapping
  public Mono<Customer> updCustomer(@RequestBody Customer customer) {
    return service.updateCustomer(customer).subscribeOn(Schedulers.elastic());
  }

  @DeleteMapping("/{id}")
  public Mono<Void> delCustomer(@PathVariable String id) {
    return service.deleteCustomer(Long.parseLong(id)).subscribeOn(Schedulers.elastic());
  }

}
