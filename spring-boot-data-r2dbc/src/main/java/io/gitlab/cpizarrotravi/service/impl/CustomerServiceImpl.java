/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.gitlab.cpizarrotravi.service.impl;

import io.gitlab.cpizarrotravi.entity.Customer;
import io.gitlab.cpizarrotravi.repository.CustomerRepository;
import io.gitlab.cpizarrotravi.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author cpizarrotravi
 */
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

  @Autowired
  private CustomerRepository repository;

  @Override
  public Mono<Customer> insertCustomer(Customer customer) {
    return repository.save(customer);
  }

  @Override
  public Mono<Customer> updateCustomer(Customer customer) {
    return repository.save(customer);
  }

  @Override
  public Mono<Void> deleteCustomer(Long id) {
    return repository.deleteById(id);
  }

  @Override
  public Mono<Customer> findCustomer(Long id) {
    return repository.findById(id);
  }

  @Override
  public Flux<Customer> findAllCustomer() {
    return repository.findAll();
  }

}
