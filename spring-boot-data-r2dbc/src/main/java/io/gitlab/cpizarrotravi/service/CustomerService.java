/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.gitlab.cpizarrotravi.service;

import io.gitlab.cpizarrotravi.entity.Customer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author cpizarrotravi
 */
public interface CustomerService {

  Mono<Customer> insertCustomer(Customer customer);

  Mono<Customer> updateCustomer(Customer customer);

  Mono<Void> deleteCustomer(Long id);

  Mono<Customer> findCustomer(Long id);

  Flux<Customer> findAllCustomer();

}
