package io.gitlab.cpizarrotravi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDataR2dbcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDataR2dbcApplication.class, args);
	}

}
