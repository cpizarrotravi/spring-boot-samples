# spring-boot-samples

- [spring-boot-hello-world](https://gitlab.com/cpizarrotravi/spring-boot-samples/-/tree/master/spring-boot-hello-world)
- [spring-boot-web](https://gitlab.com/cpizarrotravi/spring-boot-samples/-/tree/master/spring-boot-web)
- [spring-boot-webflux](https://gitlab.com/cpizarrotravi/spring-boot-samples/-/tree/master/spring-boot-webflux)
- [spring-boot-data-jdbc](https://gitlab.com/cpizarrotravi/spring-boot-samples/-/tree/master/spring-boot-data-jdbc)
- [spring-boot-data-r2dbc](https://gitlab.com/cpizarrotravi/spring-boot-samples/-/tree/master/spring-boot-data-r2dbc)