package io.gitlab.cpizarrotravi;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 *
 * @author cpizarrotravi
 */
@SpringBootTest
class SpringBootWebApplicationTests {

  @Test
  void contextLoads() {
  }

}
