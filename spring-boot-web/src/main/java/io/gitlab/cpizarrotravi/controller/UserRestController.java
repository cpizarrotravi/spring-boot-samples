/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.gitlab.cpizarrotravi.controller;

import io.gitlab.cpizarrotravi.model.User;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cpizarrotravi
 */
@RestController
@RequestMapping("/user")
public class UserRestController {

  private final AtomicLong index = new AtomicLong(1);
  private final List<User> users = new ArrayList<>();

  @GetMapping
  public List<User> listUser() {
    return users;
  }

  @GetMapping("/{id}")
  public User getUser(@PathVariable String id) {
    return users.stream().filter(u -> Long.parseLong(id) == u.getId()).findFirst().orElse(null);
  }

  @PostMapping
  public User newUser(@RequestBody User newUser) {
    newUser.setId(index.getAndIncrement());
    users.add(newUser);
    return newUser;
  }

  @PutMapping
  public User updUser(@RequestBody User updUser) {
    int position;
    for (position = 0; position < users.size(); position++) {
      if (updUser.getId().longValue() == users.get(position).getId()) {
        break;
      }
    }
    users.set(position, updUser);
    return updUser;
  }

  @DeleteMapping("/{id}")
  public void delUser(@PathVariable String id) {
    users.removeIf(u -> Long.parseLong(id) == u.getId());
  }

}
