/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.gitlab.cpizarrotravi.model;

import lombok.Data;

/**
 *
 * @author cpizarrotravi
 */
@Data
public class User {

  private Long id;
  private String username;
  private String email;

}
