package io.gitlab.cpizarrotravi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @author cpizarrotravi
 */
@SpringBootApplication
public class SpringBootDataJdbcApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringBootDataJdbcApplication.class, args);
  }

}
