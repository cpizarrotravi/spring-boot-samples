/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.gitlab.cpizarrotravi.controller;

import io.gitlab.cpizarrotravi.entity.Customer;
import io.gitlab.cpizarrotravi.service.CustomerService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cpizarrotravi
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {

  @Autowired
  private CustomerService service;

  @GetMapping
  public List<Customer> listCustomer() {
    return service.findAllCustomer();
  }

  @GetMapping("/{id}")
  public Customer getCustomer(@PathVariable String id) {
    return service.findCustomer(Long.parseLong(id));
  }

  @PostMapping
  public Customer newCustomer(@RequestBody Customer customer) {
    service.insertCustomer(customer);
    return customer;
  }

  @PutMapping
  public void updCustomer(@RequestBody Customer customer) {
    service.updateCustomer(customer);
  }

  @DeleteMapping("/{id}")
  public void delCustomer(@PathVariable String id) {
    service.deleteCustomer(Long.parseLong(id));
  }

}
