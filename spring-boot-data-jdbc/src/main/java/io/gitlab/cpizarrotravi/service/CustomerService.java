/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.gitlab.cpizarrotravi.service;

import io.gitlab.cpizarrotravi.entity.Customer;
import java.util.List;

/**
 *
 * @author cpizarrotravi
 */
public interface CustomerService {

  void insertCustomer(Customer customer);

  void updateCustomer(Customer customer);

  void deleteCustomer(Long id);

  Customer findCustomer(Long id);

  List<Customer> findAllCustomer();

}
