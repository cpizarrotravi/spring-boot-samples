/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.gitlab.cpizarrotravi.service.impl;

import io.gitlab.cpizarrotravi.entity.Customer;
import io.gitlab.cpizarrotravi.repository.CustomerRepository;
import io.gitlab.cpizarrotravi.service.CustomerService;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cpizarrotravi
 */
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

  @Autowired
  private CustomerRepository repository;

  @Override
  public void insertCustomer(Customer customer) {
    repository.save(customer);
  }

  @Override
  public void updateCustomer(Customer customer) {
    repository.save(customer);
  }

  @Override
  public void deleteCustomer(Long id) {
    repository.deleteById(id);
  }

  @Override
  public Customer findCustomer(Long id) {
    return repository.findById(id).orElse(null);
  }

  @Override
  public List<Customer> findAllCustomer() {
    return StreamSupport.stream(repository.findAll().spliterator(), false)
            .collect(Collectors.toList());
  }

}
