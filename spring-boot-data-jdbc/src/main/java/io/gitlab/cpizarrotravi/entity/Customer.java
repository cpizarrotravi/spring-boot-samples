/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.gitlab.cpizarrotravi.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

/**
 *
 * @author cpizarrotravi
 */
@Data
@Table
public class Customer {

  @Id
  private Long id;
  private String firstName;
  private String lastName;

}
